import React, { useCallback } from 'react'



const Todos=({todos,deletefunc}) =>{
    const todoList=todos.length? (
        todos.map(todo => {
            return(
                <div className="collection-item" key={todo.id}>
                <p>{todo.content}      <button onClick={()=>{deletefunc(todo.id)}}>Task Completed</button></p>
                </div>
            );
        })
    ):(
        <p>You have no todos left</p>
    );
    return (
        <div className="todos collection">
        {todoList}
        </div>
    );
}
export default Todos